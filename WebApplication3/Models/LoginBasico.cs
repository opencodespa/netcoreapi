﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
 
namespace WebApplication3.Models
{
    public class LoginBasico
    {
        public int Id { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        internal Appdb Database { get; set; }

        public LoginBasico()
        {
            //
        }

        internal LoginBasico(Appdb db)
        {
            Database = db;
        }
        //inserta nuevo usuario ..
        public async Task Crear_Usuario_Async()
        {
            using var cmd = Database.Connection.CreateCommand();
            cmd.CommandText = @"INSERT INTO `usuarios` (`email`, `passwd`) VALUES (@email, @password);";
            cmd.Parameters.AddWithValue("@email", Email);
            cmd.Parameters.AddWithValue("@password", Password);
            await cmd.ExecuteNonQueryAsync();
            Id = (int)cmd.LastInsertedId;
        }

        public async Task Cambiar_Password_Async()
        {
            using var cmd = Database.Connection.CreateCommand();
            cmd.CommandText = @"UPDATE `usuarios` SET `passwd` = @password WHERE `Id` = @id;";
            cmd.Parameters.AddWithValue("@password", Password);
            cmd.Parameters.AddWithValue("@id", Id);
            await cmd.ExecuteNonQueryAsync();
        }
        public async Task<LoginBasico> Leer_Usuario_Asyc()
        {
            using var cmd = Database.Connection.CreateCommand();
            cmd.CommandText = @"SELECT `id`, `email`, `passwd` FROM `usuarios` WHERE `email` = @email AND `passwd` = @clave ";
            cmd.Parameters.AddWithValue("@email", Email);
            cmd.Parameters.AddWithValue("@password", Password);
            var result = await ReadAllAsync(await cmd.ExecuteReaderAsync());
            return result.Count > 0 ? result[0] : null;
        }

        public async Task Eliminar_Usuario_Async()
        {
            using var cmd = Database.Connection.CreateCommand();
            cmd.CommandText = @"DELETE FROM  `usuarios` WHERE `Id` = @id;";
            cmd.Parameters.AddWithValue("@id", Id);
            await cmd.ExecuteNonQueryAsync();
        }
        private async Task<List<LoginBasico>> ReadAllAsync(DbDataReader read)
        {
            var posts = new List<LoginBasico>();
            using (read)
            {
                while (await read.ReadAsync())
                {
                    var post = new LoginBasico(Database)
                    {
                        Id = read.GetInt32(0),
                        Email = read.GetString(1),
                        Password = read.GetString(2),
                    };
                    //
                    posts.Add(post);
                }

            }

            return posts;
        }
    }
}