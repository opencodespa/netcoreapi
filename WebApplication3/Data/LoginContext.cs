﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace WebApplication3.Data
{
    public class LoginContext : DbContext
    {

        public LoginContext(DbContextOptions<LoginContext> options)
            : base(options)
        {
            //
        }

        public DbSet<WebApplication3.Models.LoginBasico> LoginBasico { get; set; }
    }
}
