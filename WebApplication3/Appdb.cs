﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace WebApplication3
{
    public class Appdb : IDisposable
    {
        public MySqlConnection Connection { get; }

        public Appdb(string conn)
        {
            Connection = new MySqlConnection(conn);
        }

        public void Dispose() => Connection.Dispose();
    }
}