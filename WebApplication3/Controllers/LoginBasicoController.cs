﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApplication3.Models;

namespace WebApplication3.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginBasicoController : ControllerBase
    {
        public Appdb Database { get; }
        public LoginBasicoController(Appdb db)
        {
            Database = db;
        }

         //Crea sesion de login
        //GET api/blog/5
        [HttpGet("{email}/{password}")]
        public async Task<IActionResult> Leer_usuario(string email, string password) 
        {
            await Database.Connection.OpenAsync();
            var query = new LoginBasico(Database);
            query.Email = email;
            query.Password = password;
            var result = await query.Leer_Usuario_Asyc();
            if (result is null)
            {
                return new NotFoundResult();
            }
            
            //
            return new OkObjectResult(result);        
        }

        //POST NEW USER
        //POST api/blog
        [HttpPost]
        public async Task<IActionResult> Crear_Usuario([FromBody] LoginBasico body) 
        {
            await Database.Connection.OpenAsync();
            body.Database = Database;
            await body.Crear_Usuario_Async();
            return new OkObjectResult(body);
        }

        //Actualiza Usuario
        //PUT api/blog/5
        [HttpPut("{email}/{password}")]
        public async Task<IActionResult> Cambiar_Password(int id, string password, [FromBody] LoginBasico body) 
        {
            await Database.Connection.OpenAsync();
            var query = new LoginBasico(Database);
            query.Id = id;
            query.Password = password;
            var result = await query.Leer_Usuario_Asyc();
            if (result is null)
             {
                return new NotFoundResult();        
             }
            result.Email = body.Email;
            result.Password = body.Password;
            await result.Cambiar_Password_Async();
            return new OkObjectResult(result);
        }

        //DELETE api/login/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Eliminar_Usuario(int id) 
        {
            await Database.Connection.OpenAsync();
            var query = new LoginBasico(Database);
            query.Id = id;
            await query.Eliminar_Usuario_Async();
            return new OkResult();
        }

     }
}